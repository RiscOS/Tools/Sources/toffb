#
# Copyright (c) 2020, RISC OS Open Ltd
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of RISC OS Open Ltd nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Makefile for toffb

COMPONENT ?= toffb

OBJS       = toffb lex
ifneq (Host,${APCS})
OBJS      += ro_support
endif
ifeq (GNU,${TOOLCHAIN})
CFLAGS    += -Wno-missing-field-initializers -Wno-sign-compare -D_POSIX_SOURCE
else
CFLAGS    += -D__GNUC__=0
endif
SOURCES_TO_SYMLINK = $(wildcard l/*)

include CApp

ifeq (,${MAKE_VERSION})

c.lex: l.lex
	${LEX} ${LFLAGS} -t l.lex > $@

o.lex: c.lex
	${CC} ${CFLAGS} -o $@ c.lex

clean::
	${RM} c.lex

else

lex.c: lex.l
	${LEX} ${LFLAGS} -t $< > $@

endif

# Dynamic dependencies:
