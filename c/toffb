/*
 * Copyright (c) 2020, RISC OS Open Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of RISC OS Open Ltd nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include "toffb.h"
#include "VersionNum"

tok_t *tok_head, *tok_tail;

static char line_buffer[256] = "\r";
static char *p = line_buffer + 1;
static FILE *out;

static int parse_int(const char *string, bool *syntax_error)
{
  int value;
  char *endptr;
  value = strtol(string, &endptr, 0);
  if (*string == '\0' || *endptr != '\0')
    *syntax_error = true;
  return value;
}

/**
 * Check for presence of a given command-line option.
 * \arg pargv        Reference to command line argv, updated on exit.
 * \arg short_form   Single-letter version of switch, no leading hyphen.
 * \arg long_form    Multi-letter version of switch, no leading hyphens.
 * \arg value        If NULL, option does not have an associated value. Otherwise, pointer through which to return value.
 * \arg parse        If value is non-NULL, use this function to parse the value.
 * \arg syntax_error Write 'true' via this pointer is a syntax error occurred.
 * \return Truth value indicating whether the specified option was found.
 */
static bool check_argument(char ***pargv, const char *short_form, const char *long_form, int *value, int (*parse)(), bool *syntax_error)
{
  bool found = false;
  const char *p = **pargv;
  if (*p == '-')
  {
    ++p;
    if (short_form && *p == *short_form)
    {
      ++p;
      found = true;
      if (value && *p == '\0')
        p = *++*pargv;
    }
    else if (long_form && *p == '-' && strcmp(p + 1, long_form) == 0)
    {
      found = true;
      if (value)
        p = *++*pargv;
    }
    if (found)
    {
      if (value)
      {
        if (p == NULL)
          *syntax_error = true;
        else
          *value = parse(p, syntax_error);
      }
      ++*pargv;
    }
  }
  return found;
}

static void output(void)
{
  if (fwrite(line_buffer, 1, p - line_buffer, out) != p - line_buffer)
  {
    fprintf(stderr, "Error writing to output file\n");
    exit(EXIT_FAILURE);
  }
}

static inline int iswordc(int c)
{
  return isalnum(c) || c == '_' || c == '`';
}

int main(int argc, char *argv[])
{
  const char *exe = *argv++;
  bool syntax_error = false;
  int status = EXIT_FAILURE;
  int crunch = 0;
  out = stdout;

  /* Parse command line options */
  (void) argc;
  while (*argv != NULL && !syntax_error)
  {
    if (check_argument(&argv, "h", "help", NULL, NULL, &syntax_error))
    {
      /* Behave as though found a syntax error */
      syntax_error = true;
      /* But return success instead */
      status = EXIT_SUCCESS;
    }
    else if (check_argument(&argv, "c", "crunch", &crunch, parse_int, &syntax_error))
    {
      /* Nothing else to do */
    }
    else if (**argv == '-')
    {
      /* Unrecognised option */
      syntax_error = true;
    }
    else if (yyin == NULL)
    {
      /* First non-switch option is input filename */
      yyin = fopen(*(argv++), "r");
      if (yyin == NULL)
      {
        perror("Cannot open input file");
        exit(EXIT_FAILURE);
      }
    }
    else if (out == stdout)
    {
      /* Second non-switch option is output filename */
      out = fopen(*(argv++), "wb");
      if (out == NULL)
      {
        perror("Cannot open output file");
        exit(EXIT_FAILURE);
      }
    }
    else
    {
      /* Too many non-switch options */
      syntax_error = true;
    }
  }
  if (syntax_error)
  {
    fprintf(stderr,
            "toffb " Module_HelpVersion "\n"
            "Syntax: %s [<options>] [<input_file>] [<output_file>]\n"
            "Options:\n"
            "-h,        --help            Show this message\n"
            "-c<level>, --crunch <level>  Set crunch options (default 0)\n",
            exe);
    exit(status);
  }

  bool first_line = true;
  uint16_t line_number = 10;

  /* Output initial CR character */
  output();

  for (;;)
  {
    tok_t *tok, *prev;

    /* Call yylex() to lex one line of input, returning results in tok_head/tok_tail. */
    yylex();

    if (tok_head == NULL)
      break;

    if (line_number == 0xFF00)
    {
      fprintf(stderr, "Too many lines\n");
      exit(EXIT_FAILURE);
    }

    p = line_buffer;
    /* Insert line number (big-endian) */
    *p++ = line_number >> 8;
    *p++ = line_number++;
    /* Skip length byte to 0 for now */
    ++p;

    for (prev = NULL, tok = tok_head; tok != NULL; tok = tok->next)
    {
      unsigned char before = p[-1], after = tok->next->value[0];
      if (tok->type == TOK_LEADING_SPACE && (crunch & CRUNCH_LEADINGSPACES))
        continue;
      if (tok->type == TOK_SPACE && (crunch & CRUNCH_SPACES))
      {
        /* Handle exceptions where one space character must remain */
        if ((before == '"' && after == '"') ||
            ((before == '$' || before == '%' || before == TRND) && (after == '(' || after == '!' || after == '?')) ||
            ((before == TEOR || before == TAND) && iswordc(after)) ||
            (before == ')' && (after == '!' || after == '?')) ||
            ((iswordc(before) || before == '.') && (iswordc(after) || after == '.' || after == '$' || after == '(' || after == '!' || after == '?')))
        {
          if (p + 1 > line_buffer + sizeof line_buffer)
          {
            fprintf(stderr, "Line too long\n");
            exit(EXIT_FAILURE);
          }
          *p++ = ' ';
          prev = tok;
        }
        continue;
      }
      if (tok->type == TOK_COMMENT && (crunch & CRUNCH_COMMENTS) && !first_line)
        continue;
      if (tok->type == TOK_STMT_BOUND && (crunch & CRUNCH_EMPTYSTMTS) && prev && prev->type == TOK_STMT_BOUND && before == ':')
        --p;
      if (p + tok->size > line_buffer + sizeof line_buffer)
      {
        fprintf(stderr, "Line too long\n");
        exit(EXIT_FAILURE);
      }
      memcpy(p, tok->value, tok->size);
      p += tok->size;
      prev = tok;
    }

    /* Go back and fill in the length byte */
    line_buffer[2] = p - line_buffer;

    /* Line is now ready to output */
    if (p - line_buffer > 4 || (crunch & CRUNCH_EMPTYLINES) == 0)
      output();

    /* Delete the chain. */
    for (tok = tok_head; tok != NULL; )
    {
      tok_t *next = tok->next;
      free(tok);
      tok = next;
    }
    tok_head = tok_tail = NULL;
    first_line = false;
  }

  /* Output program terminator byte */
  p = line_buffer;
  *p++ = 255;
  output();

  fclose(out);
}

int yywrap(void)
{
  return 1; // no next file
}
